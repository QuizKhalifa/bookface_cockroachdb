# Description

The deployment strategi presented by Kyrre, and the CockroachDB  documentation, does not work for us. This is because of a network issue, probably DNS, as extracted from the logs.

To fix this issue, one must change the fundemental deployment model that CockroachDB suggests. In its documentation, CockroachDB  tells users to deploy three seperate services. This is done because of DNS, however, this creates problems for us. What we want to do is to deploy ONE service that we scale up to host three instances. There are several bonuses to having such a deployment strategy: easier scaling, having the same volume name on all hosts, etc.; however we only care about the possibility to use global mode. This is to ensure that the same instance will start on the same node every time. (As of writing this I realise that this is not strictly a necessity, but it is necessary in the way documentet by Kyrre, but was difficult to implement, and it didn’t solve the underlying problem)

But then I hear you ask: “But what about DNS? Won’t just having one service make the instance try to connect to itself over and over again, creating the same issue as above?”, and to that I say: “Yes, that is correct, however there is a solution to that: haproxy”. By saying that the database instances should join the haproxy service on port 26257, and haproxy only points to the RoachDB service, haproxy will balance the connections so that the databases can connect to each other!

An issue that arrises at this point is that, if you’re using global mode, all the nodes may not have joined when you initialized the cluster! This can be fixed by finding the offending instance and killing it over and over again until it joins the cluster. 

If you used replica mode, and you launched one replica, there should be no problems in scaling the amount of replicas (after you’ve initilized the cluster) and having them join the cluster. New instances should try to search for a cluster to join over and over again until it finds one.

Scaling the cluster should now be a breeze. With global mode, just add a new node to the swarm, and with replicas mode, add a new node, and then scale the service.