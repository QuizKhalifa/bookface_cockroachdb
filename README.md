# Setting up a cockroachDB cluster in a Docker swarm as a bookface backend

This tutorial describes how to set up a **stable** database cluster using cockroachDB and docker swarm.

**NOTE**: This is only necessary if you're having problems with the stability of CockroachDB in swarm mode ( e.g. it depends on one instance being up )

See [this](https://bitbucket.org/QuizKhalifa/bookface_cockroachdb/src/master/WHY.md) to see why this works, and a detailed explenation of what to do.

*This tutorial is copied and modified from [Kyrres Gitlab page](https://git.cs.hioa.no/kyrre.begnum/cockroachdb_cluster_bookface/blob/master/README.md)*

## Prerequisites

* Docker is installed and initialized as a swarm ( this can work on a swarm with a single host, but not optimally ) ( I, Bror, would recommend having three swarm masters, as you need three instances anyway to utilize CockroachDBs fault tolerance )

* Git is installed

## Set up the cluster

Clone this repository to your swarm master:
```
git clone https://QuizKhalifa@bitbucket.org/QuizKhalifa/bookface_cockroachdb.git
```
Next, enter the directory and start the entire stack: 
( Note: If you want to use replica mode instead of global mode, replace ```mode: global``` with ```replicas: 1``` in the docker compose file )

```
docker stack deploy -c docker-compose.yml roach
```

## Global mode

You now have one CockroachDB service and one haproxy service, with nr. of instances matching the amount of swarm members.

Make sure that the instances are completely set up. Run ```docker service ls``` and you should see something like this:
```
sej19syg4bz2        roach_cockroachdb   global              3/3                 cockroachdb/cockroach:v2.1.6          *:8082->8080/tcp
qun5aeo7xwin        roach_db-proxy      global              3/3                 haproxy:alpine
```

Make sure it says 3/3

### You can now initialize the database:

```
docker run -it --rm --network=roach_cockroachdb cockroachdb/cockroach:v2.1.6 init --host=cockroachdb --insecure
```


## Replicas mode

You should now have one CockroachDB service and one haproxy service with one instance each.

Make sure that the instances are completely set up. Run ```docker service ls``` and you should see something like this:
```
sej19syg4bz2        roach_cockroachdb   global              1/1                 cockroachdb/cockroach:v2.1.6          *:8082->8080/tcp
qun5aeo7xwin        roach_db-proxy      global              1/1                 haproxy:alpine
```
Make sure it says 1/1

### You can now initialize the database:

```
docker run -it --rm --network=roach_cockroachdb cockroachdb/cockroach:v2.1.6 init --host=cockroachdb --insecure
```

Now that the database is initialized, you can scale the service to include more instances:
```
docker service scale roach_cockroachdb=3
docker service scale roach_db-proxy=3
```

# Create the database

## Check the dashboard

CockroachDB comes with it's own web-based dashboard. You can check the status by using the port 8080. ( You probably don't want to have this one open to all, so be careful with your security groups etc. )

```
http://ip-to-swarm-node:8082
```

Make sure the new instances joined your cluster. If they didn't, kill the offending containers until they connect:
```
docker ps
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS              PORTS                 NAMES
5696e892a5aa        cockroachdb/cockroach:v2.1.6          "/cockroach/cockroac…"   4 hours ago         Up 4 hours          8080/tcp, 26257/tcp   roach_cockroachdb.tiwuq185vrbhczdqu68396y2u.nr90ev6dz6onqjqnmehw63b72
3666983fc2cf        haproxy:alpine                        "/docker-entrypoint.…"   4 hours ago         Up 4 hours                                roach_db-proxy.tiwuq185vrbhczdqu68396y2u.kxyu81dpwus7xou2cwiekokrt
```
```
docker kill roach_cockroachdb.tiwuq185vrbhczdqu68396y2u.nr90ev6dz6onqjqnmehw63b72 
```
## Connect to the database

To interact with the database you have to run a specialized docker container.

```
docker run -it --rm --network=roach_cockroachdb cockroachdb/cockroach:v2.1.6 sql --host=cockroachdb --insecure
```

You should now se a promt from the database itself. You can now create the database, user and initialize the tables. Run these commands inside the DB promt:

```
CREATE DATABASE bf;
CREATE USER bfuser;
GRANT ALL ON DATABASE bf TO bfuser;
USE bf;
CREATE table users ( userID INT PRIMARY KEY DEFAULT unique_rowid(), name STRING(50), picture STRING(300), status STRING(10), posts INT, comments INT, lastPostDate TIMESTAMP DEFAULT NOW(), createDate TIMESTAMP DEFAULT NOW());
CREATE table posts ( postID INT PRIMARY KEY DEFAULT unique_rowid(), userID INT, text STRING(300), name STRING(150), postDate TIMESTAMP DEFAULT NOW());
CREATE table comments ( commentID INT PRIMARY KEY DEFAULT unique_rowid(), postID INT, userID INT, text STRING(300),  postDate TIMESTAMP DEFAULT NOW());
```

## Test with a webserver

It's time to run the bookface webservers as a swarm-based service with replicas.

Start with cloning the latest version of the bookface repository: 

```
git clone https://git.cs.hioa.no/kyrre.begnum/bookface.git
```
This repository contains the files you need to build your own docker container for bookface. It also contains a docker-compose.yml file you can use to launch the entire service.

Right now, the service will dowload a prebuilt image, but if you build your own, make sure to make modify the compose fil to point to your own image.

```
docker stack deploy -c docker-compose.yml --with-registry-auth bf
```



## Send some traffic

This specialized image can create some users ( without images for now ) and will also generate posts and comments if you let it run for a while. 

BE SURE TO UPDATE THE ENTRYPOINT AND PORT! If you are unsure, just use curl to test if you get something. Use the following to see the port that bookface exposes.

```
docker service ls | grep bf
```

Then try with curl: 

```
curl http://an-ip-you-think-will-work:port-you-belive-is-right/index.php

```

When you feel ready, run the container with the usage script:

```
docker run -ti --rm -e NOWAIT=1 -e ENTRYPOINT=192.168.128.5:30004 -e INTERVAL=2 docker.cs.hioa.no/kyrrepublic/simplewebuser
```
